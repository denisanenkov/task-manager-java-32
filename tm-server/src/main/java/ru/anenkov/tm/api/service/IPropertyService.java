package ru.anenkov.tm.api.service;

import java.io.IOException;

public interface IPropertyService {

    void init() throws IOException;

    String getServerHost();

    Integer getServerPort();

    String getSessionSalt();

    Integer getSessionCycle();

    String getURL();

    String getLoginDB();

    String getPassDB();

    String getDriverDB();

    String getTimezone();

    String getDialect();

    String getDDL();

    String getSqlShowSettings();

}
