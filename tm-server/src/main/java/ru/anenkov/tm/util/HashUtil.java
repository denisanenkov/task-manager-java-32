package ru.anenkov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.exception.empty.EmptyPasswordException;

public interface HashUtil {

    @NotNull String SECRET = "12345698765445";

    @NotNull Integer ITERATION = 47641;

    @Nullable
    static String salt(@Nullable final String value) {
        if (value == null) throw new EmptyPasswordException();
        @Nullable String result = value;
        for (int i = 0; i < ITERATION; i++) {
            result = MD5(SECRET + value + SECRET);
        }
        return result;
    }

    @Nullable
    static String MD5(@Nullable final String md5) {
        if (md5 == null) return null;
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            final byte[] array = md.digest(md5.getBytes());
            @NotNull final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}
