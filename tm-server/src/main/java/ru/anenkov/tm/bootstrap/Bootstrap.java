package ru.anenkov.tm.bootstrap;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.api.repository.*;
import ru.anenkov.tm.api.service.*;
import ru.anenkov.tm.constant.DataConst;
import ru.anenkov.tm.constant.MessageConst;
import ru.anenkov.tm.endpoint.*;
import ru.anenkov.tm.repository.*;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.service.*;
import ru.anenkov.tm.util.EntityManagerUtil;

import javax.xml.ws.Endpoint;
import java.io.IOException;
import java.util.List;

@Component
@NoArgsConstructor
public final class Bootstrap {

    @Autowired
    private UserService userService;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private AbstractEndpoint[] endpointList;

    private void initEndpoint() {
        for (@Nullable final AbstractEndpoint endpoint : endpointList) {
            registryEndpoint(endpoint);
        }
    }

    private void registryEndpoint(final Object endpoint) {
        if (endpoint == null) return;
        final String host = DataConst.SERVER_HOST;
        final String port = DataConst.SERVER_PORT;
        final String name = endpoint.getClass().getSimpleName();
        final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        Endpoint.publish(wsdl, endpoint);
        System.out.println(wsdl);
    }

    public void run(@Nullable final String[] args) throws Exception {
        initEndpoint();
        System.out.println("\t\t***SERVERS*STARTED***");
        if (args == null || args.length != 0) System.exit(0);
    }

    public void initUsers() {
        userService.create("1", "1", "test@mail.ru");
        userService.create("test", "test", "test@mail.ru");
        userService.create("admin", "admin", Role.ADMIN);
        userService.create("2", "2", "den@mail.ru");
    }

}