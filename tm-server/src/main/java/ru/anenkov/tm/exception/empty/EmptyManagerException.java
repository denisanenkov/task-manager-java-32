package ru.anenkov.tm.exception.empty;

import ru.anenkov.tm.exception.AbstractException;

public class EmptyManagerException extends AbstractException {

    public EmptyManagerException() {
        super("Error! Problems with connecting to the database..");
    }

}
