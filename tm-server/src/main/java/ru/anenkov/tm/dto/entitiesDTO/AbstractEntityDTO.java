package ru.anenkov.tm.dto.entitiesDTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@MappedSuperclass
public abstract class AbstractEntityDTO implements Serializable {

    public static final long serialVersionUID = 1L;

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();


}
