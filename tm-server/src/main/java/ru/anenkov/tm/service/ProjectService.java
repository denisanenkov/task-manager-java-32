package ru.anenkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.anenkov.tm.api.repository.IProjectRepository;
import ru.anenkov.tm.api.service.IProjectService;
import ru.anenkov.tm.dto.entitiesDTO.ProjectDTO;
import ru.anenkov.tm.entity.Project;
import ru.anenkov.tm.exception.empty.*;
import ru.anenkov.tm.exception.system.IncorrectIndexException;
import ru.anenkov.tm.exception.user.EntityConvertException;
import ru.anenkov.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @Override
    @SneakyThrows
    public Project toProject(@Nullable final String userId, @Nullable final ProjectDTO projectDTO) {
        if (projectDTO == null) throw new EmptyEntityException();
        @Nullable final Project project = findOneByIdEntity(userId, projectDTO.getId());
        if (project == null) throw new EntityConvertException();
        return project;
    }

    @Override
    @SneakyThrows
    public ProjectDTO toProjectDTO(@Nullable final Project project) {
        @Nullable ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setUserId(project.getUser().getId());
        projectDTO.setName(project.getName());
        projectDTO.setDescription(project.getDescription());
        return projectDTO;
    }

    @Override
    @SneakyThrows
    public List<ProjectDTO> toProjectDTOList(@Nullable final List<Project> projectList) {
        if (projectList == null || projectList.isEmpty()) throw new EmptyListException();
        @Nullable List<ProjectDTO> projectDTOS = new ArrayList<>();
        for (Project projectEntity : projectList) {
            @Nullable ProjectDTO projectDTO = new ProjectDTO();
            projectDTO.setUserId(projectEntity.getUser().getId());
            projectDTO.setName(projectEntity.getName());
            projectDTO.setDescription(projectEntity.getDescription());
            projectDTOS.add(projectDTO);
        }
        return projectDTOS;
    }

    @Override
    @SneakyThrows
    public void create(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = new Project();
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            projectRepository.startTransaction();
            project.setName(name);
            projectRepository.add(userId, project);
            projectRepository.commitTransaction();
        } catch (Exception ex) {
            projectRepository.rollbackTransaction();
            throw ex;
        } finally {
            projectRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public void create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = new Project();
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            projectRepository.startTransaction();
            project.setName(name);
            project.setDescription(description);
            projectRepository.add(userId, project);
            projectRepository.commitTransaction();
        } catch (Exception ex) {
            projectRepository.rollbackTransaction();
            throw ex;
        } finally {
            projectRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public void add(
            @Nullable final String userId,
            @Nullable final Project project
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) return;
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            projectRepository.startTransaction();
            projectRepository.add(userId, project);
            projectRepository.add(userId, project);
            projectRepository.commitTransaction();
        } catch (Exception ex) {
            projectRepository.rollbackTransaction();
            throw ex;
        } finally {
            projectRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public void remove(
            @Nullable final String userId,
            @Nullable final Project project
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) throw new EmptyEntityException();
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            projectRepository.startTransaction();
            @Nullable final String taskId = project.getId();
            projectRepository.removeOneById(userId, taskId);
            projectRepository.commitTransaction();
        } catch (Exception ex) {
            projectRepository.rollbackTransaction();
            throw ex;
        } finally {
            projectRepository.closeTransaction();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAllEntities(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            projectRepository.startTransaction();
            @Nullable final List<Project> projects = projectRepository.findAll(userId);
            projectRepository.commitTransaction();
            return projects;
        } catch (Exception ex) {
            projectRepository.rollbackTransaction();
            throw ex;
        } finally {
            projectRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            projectRepository.startTransaction();
            projectRepository.clear(userId);
            projectRepository.commitTransaction();
        } catch (Exception ex) {
            projectRepository.rollbackTransaction();
            throw ex;
        } finally {
            projectRepository.closeTransaction();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findOneByIndexDTO(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            projectRepository.startTransaction();
            @Nullable final ProjectDTO project = projectRepository.findOneByIndexDTO(userId, index);
            projectRepository.commitTransaction();
            return project;
        } catch (Exception ex) {
            projectRepository.rollbackTransaction();
            throw ex;
        } finally {
            projectRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public @Nullable ProjectDTO findOneByIdDTO(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            projectRepository.startTransaction();
            @Nullable final ProjectDTO project = projectRepository.findOneByIdDTO(userId, id);
            projectRepository.commitTransaction();
            return project;
        } catch (Exception ex) {
            projectRepository.rollbackTransaction();
            throw ex;
        } finally {
            projectRepository.closeTransaction();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findOneByNameDTO(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            projectRepository.startTransaction();
            @Nullable final ProjectDTO project = projectRepository.findOneByNameDTO(userId, name);
            projectRepository.commitTransaction();
            return project;
        } catch (Exception ex) {
            projectRepository.rollbackTransaction();
            throw ex;
        } finally {
            projectRepository.closeTransaction();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByIdEntity(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            projectRepository.startTransaction();
            @Nullable final Project project = projectRepository.findOneByIdEntity(userId, id);
            projectRepository.commitTransaction();
            return project;
        } catch (Exception ex) {
            projectRepository.rollbackTransaction();
            throw ex;
        } finally {
            projectRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public @Nullable Project findOneByIndexEntity(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            projectRepository.startTransaction();
            @Nullable final Project project = projectRepository.findOneByIndexEntity(userId, index);
            projectRepository.commitTransaction();
            return project;
        } catch (Exception ex) {
            projectRepository.rollbackTransaction();
            throw ex;
        } finally {
            projectRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public @Nullable Project findOneByNameEntity(@Nullable String userId, String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            projectRepository.startTransaction();
            @Nullable final Project project = projectRepository.findOneByNameEntity(userId, name);
            projectRepository.commitTransaction();
            return project;
        } catch (Exception ex) {
            projectRepository.rollbackTransaction();
            throw ex;
        } finally {
            projectRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public void removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            projectRepository.startTransaction();
            projectRepository.removeOneByIndex(userId, index);
            projectRepository.commitTransaction();
        } catch (Exception ex) {
            projectRepository.rollbackTransaction();
            throw ex;
        } finally {
            projectRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public void removeOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            projectRepository.startTransaction();
            projectRepository.removeOneByName(userId, name);
            projectRepository.commitTransaction();
        } catch (Exception ex) {
            projectRepository.rollbackTransaction();
            throw ex;
        } finally {
            projectRepository.closeTransaction();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            projectRepository.startTransaction();
            projectRepository.removeOneById(userId, id);
            projectRepository.commitTransaction();
        } catch (Exception ex) {
            projectRepository.rollbackTransaction();
            throw ex;
        } finally {
            projectRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public @Nullable List<ProjectDTO> findAllDTOs(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            projectRepository.startTransaction();
            List<ProjectDTO> projects = projectRepository.getListDTOs();
            projectRepository.commitTransaction();
            return projects;
        } catch (Exception ex) {
            projectRepository.rollbackTransaction();
            throw ex;
        } finally {
            projectRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public void remove(
            @Nullable final String userId,
            @Nullable final ProjectDTO projectDTO
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectDTO == null) throw new EmptyIdException();
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            projectRepository.startTransaction();
            projectRepository.remove(new ProjectService().toProject(userId, projectDTO));
            projectRepository.commitTransaction();
        } catch (Exception ex) {
            projectRepository.rollbackTransaction();
            throw ex;
        } finally {
            projectRepository.closeTransaction();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public void updateProjectByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            projectRepository.startTransaction();
            @Nullable Project project = projectRepository.findOneByIndexEntity(userId, index);
            projectRepository.updateOneById(userId, project.getId(), name, description);
            projectRepository.commitTransaction();
        } catch (Exception ex) {
            projectRepository.rollbackTransaction();
            throw ex;
        } finally {
            projectRepository.closeTransaction();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public void updateProjectById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            projectRepository.startTransaction();
            projectRepository.updateOneById(userId, id, name, description);
            projectRepository.commitTransaction();
        } catch (Exception ex) {
            projectRepository.rollbackTransaction();
            throw ex;
        } finally {
            projectRepository.closeTransaction();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public void updateProjectByName(
            @Nullable String userId,
            @NotNull String oldName,
            @NotNull String newName,
            @NotNull String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (oldName == null || oldName.isEmpty()) throw new IncorrectIndexException();
        if (newName == null || newName.isEmpty()) throw new EmptyNameException();
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            projectRepository.startTransaction();
            projectRepository.updateOneByName(userId, oldName, newName, description);
            projectRepository.commitTransaction();
        } catch (Exception ex) {
            projectRepository.rollbackTransaction();
            throw ex;
        } finally {
            projectRepository.closeTransaction();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Project> getList() {
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            projectRepository.startTransaction();
            @Nullable final List<Project> project = projectRepository.getList();
            projectRepository.commitTransaction();
            return project;
        } catch (Exception ex) {
            projectRepository.rollbackTransaction();
            throw ex;
        } finally {
            projectRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public void load(@Nullable final List<Project> projects) {
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            projectRepository.startTransaction();
            projectRepository.load(projects);
            projectRepository.commitTransaction();
        } catch (Exception ex) {
            projectRepository.rollbackTransaction();
            throw ex;
        } finally {
            projectRepository.closeTransaction();
        }
    }

}
