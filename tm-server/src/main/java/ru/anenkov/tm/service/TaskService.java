package ru.anenkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.anenkov.tm.api.repository.ITaskRepository;
import ru.anenkov.tm.api.service.ITaskService;
import ru.anenkov.tm.dto.entitiesDTO.ProjectDTO;
import ru.anenkov.tm.entity.Project;
import ru.anenkov.tm.entity.Task;
import ru.anenkov.tm.exception.empty.EmptyEntityException;
import ru.anenkov.tm.exception.empty.EmptyIdException;
import ru.anenkov.tm.exception.empty.EmptyUserIdException;
import ru.anenkov.tm.exception.system.IncorrectIndexException;
import ru.anenkov.tm.exception.empty.EmptyNameException;
import ru.anenkov.tm.dto.entitiesDTO.TaskDTO;
import ru.anenkov.tm.exception.user.EntityConvertException;
import ru.anenkov.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public final class TaskService extends AbstractService<Task> implements ITaskService {

    @Override
    @SneakyThrows
    public Task toTask(@Nullable final String userId, @Nullable final TaskDTO taskDTO) {
        if (taskDTO == null) throw new EmptyEntityException();
        @Nullable final Task task = findOneByIdEntity(userId, taskDTO.getId());
        if (task == null) throw new EntityConvertException();
        return task;
    }

    @Override
    @SneakyThrows
    public List<Task> toTaskList(
            @Nullable final String userId,
            @Nullable final List<TaskDTO> taskDTOList
    ) {
        if (taskDTOList == null || taskDTOList.isEmpty()) throw new EmptyEntityException();
        @Nullable List<Task> result = new ArrayList<>();
        for (@Nullable final TaskDTO task : taskDTOList) {
            @Nullable Task currentTask = toTask(userId, task);
            result.add(currentTask);
        }
        if (result == null || result.isEmpty()) throw new EntityConvertException();
        return result;
    }

    @Override
    @SneakyThrows
    public TaskDTO toTaskDTO(@Nullable final Task task) {
        @Nullable TaskDTO taskDTO = new TaskDTO();
        taskDTO.setUserId(task.getUser().getId());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        return taskDTO;
    }

    @Override
    @SneakyThrows
    public List<TaskDTO> toTaskDTOList(@Nullable final List<Task> taskList) {
        @Nullable List<TaskDTO> taskDTOS = new ArrayList<>();
        for (@Nullable final Task taskEntity : taskList) {
            @Nullable TaskDTO taskDTO = new TaskDTO();
            taskDTO.setUserId(taskEntity.getUser().getId());
            taskDTO.setName(taskEntity.getName());
            taskDTO.setDescription(taskEntity.getDescription());
            taskDTOS.add(taskDTO);
        }
        return taskDTOS;
    }

    @Override
    public List<Task> getListEntities(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        try {
            taskRepository.startTransaction();
            @Nullable final List<Task> tasks = taskRepository.findAllEntities(userId);
            taskRepository.commitTransaction();
            return tasks;
        } catch (Exception ex) {
            taskRepository.rollbackTransaction();
            throw ex;
        } finally {
            taskRepository.closeTransaction();
        }
    }

    @Override
    public List<TaskDTO> getListDTOs(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        try {
            taskRepository.startTransaction();
            @Nullable final List<TaskDTO> tasks = taskRepository.findAll(userId);
            taskRepository.commitTransaction();
            return tasks;
        } catch (Exception ex) {
            taskRepository.rollbackTransaction();
            throw ex;
        } finally {
            taskRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public void create(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        try {
            taskRepository.startTransaction();
            taskRepository.create(userId, name);
            taskRepository.commitTransaction();
        } catch (Exception ex) {
            taskRepository.rollbackTransaction();
            throw ex;
        } finally {
            taskRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public void create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        try {
            taskRepository.startTransaction();
            taskRepository.create(userId, name, description);
            taskRepository.commitTransaction();
        } catch (Exception ex) {
            taskRepository.rollbackTransaction();
            throw ex;
        } finally {
            taskRepository.closeTransaction();
        }
    }

    @Override
    public void remove(
            @Nullable String userId,
            @NotNull Task task
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (task == null) throw new EmptyEntityException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        try {
            taskRepository.startTransaction();
            taskRepository.removeOneById(userId, task.getId());
            taskRepository.commitTransaction();
        } catch (Exception ex) {
            taskRepository.rollbackTransaction();
            throw ex;
        } finally {
            taskRepository.closeTransaction();
        }
    }

    @Override
    public void add(
            @Nullable String userId,
            @NotNull TaskDTO task
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (task == null) throw new EmptyEntityException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        try {
            taskRepository.startTransaction();
            taskRepository.add(userId, task);
            taskRepository.commitTransaction();
        } catch (Exception ex) {
            taskRepository.rollbackTransaction();
            throw ex;
        } finally {
            taskRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        try {
            taskRepository.startTransaction();
            @Nullable final List<TaskDTO> tasks = taskRepository.findAll(userId);
            taskRepository.commitTransaction();
            return tasks;
        } catch (Exception ex) {
            taskRepository.rollbackTransaction();
            throw ex;
        } finally {
            taskRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        try {
            taskRepository.startTransaction();
            taskRepository.clear(userId);
            taskRepository.commitTransaction();
        } catch (Exception ex) {
            taskRepository.rollbackTransaction();
            throw ex;
        } finally {
            taskRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public TaskDTO findOneByIndexDTO(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        try {
            taskRepository.startTransaction();
            @Nullable final TaskDTO task = taskRepository.findOneByIndexDTO(userId, index);
            taskRepository.commitTransaction();
            return task;
        } catch (Exception ex) {
            taskRepository.rollbackTransaction();
            throw ex;
        } finally {
            taskRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public TaskDTO findOneByNameDTO(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        try {
            taskRepository.startTransaction();
            @Nullable final TaskDTO task = taskRepository.findOneByNameDTO(userId, name);
            taskRepository.commitTransaction();
            return task;
        } catch (Exception ex) {
            taskRepository.rollbackTransaction();
            throw ex;
        } finally {
            taskRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public TaskDTO findOneByIdDTO(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        try {
            taskRepository.startTransaction();
            @Nullable final TaskDTO task = taskRepository.findOneByIdDTO(userId, id);
            taskRepository.commitTransaction();
            return task;
        } catch (Exception ex) {
            taskRepository.rollbackTransaction();
            throw ex;
        } finally {
            taskRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public Task findOneByIdEntity(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        try {
            taskRepository.startTransaction();
            @Nullable final Task task = taskRepository.findOneByIdEntity(userId, id);
            taskRepository.commitTransaction();
            return task;
        } catch (Exception ex) {
            taskRepository.rollbackTransaction();
            throw ex;
        } finally {
            taskRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public Task findOneByIndexEntity(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        try {
            taskRepository.startTransaction();
            @Nullable final Task task = taskRepository.findOneByIndexEntity(userId, index);
            taskRepository.commitTransaction();
            return task;
        } catch (Exception ex) {
            taskRepository.rollbackTransaction();
            throw ex;
        } finally {
            taskRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public Task findOneByNameEntity(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        try {
            taskRepository.startTransaction();
            @Nullable final Task task = taskRepository.findOneByNameEntity(userId, name);
            taskRepository.commitTransaction();
            return task;
        } catch (Exception ex) {
            taskRepository.rollbackTransaction();
            throw ex;
        } finally {
            taskRepository.closeTransaction();
        }
    }


    @Override
    @SneakyThrows
    public void removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        try {
            taskRepository.startTransaction();
            taskRepository.removeOneByIndex(userId, index);
            taskRepository.commitTransaction();
        } catch (Exception ex) {
            taskRepository.rollbackTransaction();
            throw ex;
        } finally {
            taskRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public void removeOneByName(
            final @Nullable String userId,
            final @Nullable String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        try {
            taskRepository.startTransaction();
            taskRepository.removeOneByName(userId, name);
            taskRepository.commitTransaction();
        } catch (Exception ex) {
            taskRepository.rollbackTransaction();
            throw ex;
        } finally {
            taskRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        try {
            taskRepository.startTransaction();
            taskRepository.removeOneById(userId, id);
            taskRepository.commitTransaction();
        } catch (Exception ex) {
            taskRepository.rollbackTransaction();
            throw ex;
        } finally {
            taskRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    @NotNull
    public void updateTaskById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @NotNull final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        try {
            taskRepository.startTransaction();
            taskRepository.updateOneById(userId, id, name, description);
            taskRepository.commitTransaction();
        } catch (Exception ex) {
            taskRepository.rollbackTransaction();
            throw ex;
        } finally {
            taskRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public void updateTaskByName(
            @Nullable final String userId,
            @Nullable final String oldName,
            @Nullable final String newName,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (oldName == null || oldName.isEmpty()) throw new IncorrectIndexException();
        if (newName == null || newName.isEmpty()) throw new EmptyNameException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        try {
            taskRepository.startTransaction();
            taskRepository.updateOneByName(userId, oldName, newName, description);
            taskRepository.commitTransaction();
        } catch (Exception ex) {
            taskRepository.rollbackTransaction();
            throw ex;
        } finally {
            taskRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public void updateTaskByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String newName,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (newName == null || newName.isEmpty()) throw new EmptyNameException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        try {
            taskRepository.startTransaction();
            taskRepository.updateOneByIndex(userId, index, newName, description);
            taskRepository.commitTransaction();
        } catch (Exception ex) {
            taskRepository.rollbackTransaction();
            throw ex;
        } finally {
            taskRepository.closeTransaction();
        }
    }

    @Override
    public void load(@Nullable final List<Task> tasks) {
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        try {
            taskRepository.startTransaction();
            taskRepository.load(tasks);
            taskRepository.commitTransaction();
        } catch (Exception ex) {
            taskRepository.rollbackTransaction();
            throw ex;
        } finally {
            taskRepository.closeTransaction();
        }
    }

    @Override
    public List<Task> getList() {
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        try {
            taskRepository.startTransaction();
            @Nullable final List<Task> tasks = taskRepository.getList();
            taskRepository.commitTransaction();
            return tasks;
        } catch (Exception ex) {
            taskRepository.rollbackTransaction();
            throw ex;
        } finally {
            taskRepository.closeTransaction();
        }
    }

}