package ru.anenkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.anenkov.tm.api.service.IPropertyService;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Service
public final class PropertyService implements IPropertyService {

    @NotNull
    private final String NAME = "/application.properties";

    @NotNull
    private final Properties properties = new Properties();

    @PostConstruct
    public void init() throws IOException {
        @Nullable final InputStream inputStream = PropertyService.class.getResourceAsStream(NAME);
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            throw e;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getServerHost() {
        @Nullable final String propertyHost = this.properties.getProperty("server.host");
        @Nullable final String envHost = System.getProperty("server.host");
        if (envHost != null) return envHost;
        return propertyHost;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Integer getServerPort() {
        @Nullable final String propertyPort = this.properties.getProperty("server.port");
        @Nullable final String envPort = System.getProperty("server.port");
        @Nullable String value = propertyPort;
        if (envPort != null) value = envPort;
        return Integer.parseInt(value);
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getSessionSalt() {
        return this.properties.getProperty("sessions.salt");
    }

    @Nullable
    @Override
    @SneakyThrows
    public Integer getSessionCycle() {
        return Integer.parseInt(this.properties.getProperty("sessions.cycle"));
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getURL() {
        @Nullable final String urlDB = this.properties.getProperty("jdbc.url");
        return urlDB;
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getLoginDB() {
        @Nullable final String username = this.properties.getProperty("jdbc.username");
        return username;
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getPassDB() {
        @Nullable final String password = this.properties.getProperty("jdbc.password");
        return password;
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getDriverDB() {
        @Nullable final String driver = this.properties.getProperty("jdbc.driver");
        return driver;
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getTimezone() {
        @Nullable final String timezone = this.properties.getProperty("jdbc.timezone");
        return timezone;
    }

    @Override
    @SneakyThrows
    public String getDialect() {
        @Nullable final String dialect = this.properties.getProperty("jdbc.dialect");
        return dialect;
    }

    @Override
    @SneakyThrows
    public String getDDL() {
        @Nullable final String property = this.properties.getProperty("jdbc.ddlauto");
        return property;
    }

    @Override
    @SneakyThrows
    public String getSqlShowSettings() {
        @Nullable final String property = this.properties.getProperty("jdbc.showsql");
        return property;
    }

}
