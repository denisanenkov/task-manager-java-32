package ru.anenkov.tm.service;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.anenkov.tm.api.service.*;
import ru.anenkov.tm.dto.entitiesDTO.Domain;

import java.io.Serializable;

@AllArgsConstructor
@Service
public final class DomainService implements IDomainService, Serializable {

    @NotNull
    @Autowired
    private final ITaskService taskService;

    @NotNull
    @Autowired
    private final IUserService userService;

    @NotNull
    @Autowired
    private final IProjectService projectService;

    @Override
    @SneakyThrows
    public void load(@Nullable final Domain domain) {
        if (domain == null) return;
        taskService.load(domain.getTasks());
        projectService.load(domain.getProjects());
    }

    @Override
    @SneakyThrows
    public void export(@Nullable final Domain domain) {
        if (domain == null) return;
        domain.setTasks(taskService.getList());
        domain.setProjects(projectService.getList());
        domain.setUsers(userService.getList());
    }

}
