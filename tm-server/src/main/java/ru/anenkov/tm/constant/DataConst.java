package ru.anenkov.tm.constant;

import org.jetbrains.annotations.NotNull;

public interface DataConst {

    String FILE_BINARY = "./data.bin";

    String FILE_BASE64 = "./database64.base64";

    String FILE_XML = "./data.xml";

    String FILE_JSON = "./data.json";

    String SERVER_PORT = "8080";

    String SERVER_HOST = "localhost";

    String PROPERTIES_PATH = "tm-server/src/main/resources/application.properties";

}
