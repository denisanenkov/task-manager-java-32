package ru.anenkov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.anenkov.tm.api.repository.ITaskRepository;
import ru.anenkov.tm.entity.Task;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.exception.empty.*;
import ru.anenkov.tm.exception.system.IncorrectDataException;
import ru.anenkov.tm.dto.entitiesDTO.TaskDTO;

import java.util.*;

@Repository
@Scope("prototype")
public final class TaskRepository extends AbstractRepository<Task, TaskDTO> implements ITaskRepository {

    @Override
    @Nullable
    @SneakyThrows
    public List<Task> getListEntities() {
        return entityManager.createQuery("SELECT e FROM Task e", Task.class)
                .getResultList();
    }

    @Override
    @SneakyThrows
    public List<TaskDTO> getListDTOs() {
        return entityManager.createQuery("SELECT e FROM TaskDTO e", TaskDTO.class)
                .getResultList();
    }

    @Override
    @SneakyThrows
    public @Nullable Long count() {
        return entityManager.createQuery("SELECT COUNT(e) FROM Task e", Long.class)
                .getSingleResult();
    }

    @Override
    @SneakyThrows
    public Task create(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        @NotNull final User user = entityManager.find(User.class, userId);
        @NotNull final Task task = new Task(name, user);
        return merge(task);
    }

    @Override
    @SneakyThrows
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        @NotNull final User user = entityManager.find(User.class, userId);
        @NotNull final Task task = new Task(name, description, user);
        return merge(task);
    }

    @Override
    @SneakyThrows
    public void add(
            @NotNull final String userId,
            @Nullable TaskDTO task
    ) {
        @NotNull final User user = entityManager.find(User.class, userId);
        task.setUserId(user.getId());
        entityManager.persist(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(
            @NotNull final String userId
    ) {
        @Nullable final List<Task> listTasks = entityManager.createQuery("SELECT e FROM Task e WHERE e.user.id = :userId", Task.class)
                .setParameter("userId", userId)
                .getResultList();
        return TaskDTO.toDTO(listTasks);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllEntities(@NotNull final String userId) {
        @Nullable final List<Task> listTasks = entityManager.createQuery("SELECT e FROM Task e WHERE e.user.id = :userId", Task.class)
                .setParameter("userId", userId)
                .getResultList();
        return listTasks;
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final List<Task> listTasks = findAllEntities(userId);
        for (@NotNull final Task task : listTasks) {
            entityManager.remove(task);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task findOneByIndexEntity(
            @NotNull final String userId,
            @Nullable final Integer index
    ) {
        @Nullable final List<Task> listTasks = entityManager.createQuery("SELECT e FROM Task e WHERE e.user.id = :userId", Task.class)
                .setParameter("userId", userId)
                .getResultList();
        if (listTasks.size() < index) throw new IncorrectDataException();
        return listTasks.get(index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneByNameEntity(
            @NotNull final String userId,
            @Nullable final String name
    ) {
        @Nullable final List<Task> listTasks = entityManager.createQuery("SELECT e FROM Task e WHERE e.user.id = :userId AND e.name = :name", Task.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1)
                .getResultList();
        return getFirstEntity(listTasks);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneByIdEntity(
            @NotNull final String userId,
            @Nullable final String id
    ) {
        @Nullable final List<Task> listTasks = entityManager.createQuery("SELECT e FROM Task e WHERE e.user.id = :userId AND e.id = :id", Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        return getFirstEntity(listTasks);
    }

    @Override
    @SneakyThrows
    public @Nullable TaskDTO findOneByIndexDTO(String userId, Integer index) {
        @Nullable final List<TaskDTO> listTasks = entityManager.createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId", TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
        if (listTasks.size() < index) throw new IncorrectDataException();
        return listTasks.get(index);
    }

    @Override
    @SneakyThrows
    public @Nullable TaskDTO findOneByNameDTO(String userId, String name) {
        @Nullable final List<TaskDTO> listTasks = entityManager.createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId AND e.name = :name", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1)
                .getResultList();
        return getFirstDTO(listTasks);
    }

    @Override
    @SneakyThrows
    public @Nullable TaskDTO findOneByIdDTO(String userId, String id) {
        @Nullable final List<TaskDTO> listTasks = entityManager.createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId AND e.id = :id", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        return getFirstDTO(listTasks);
    }

    @Override
    @SneakyThrows
    public void remove(
            @NotNull final String userId,
            @Nullable final Task task
    ) {
        if (task == null) throw new EmptyEntityException();
        entityManager.remove(task);
    }

    @Override
    @SneakyThrows
    public void removeOneByIndex(
            @NotNull final String userId,
            @Nullable final Integer index
    ) {
        if (index == null || index < 0) throw new EmptyIdException();
        @Nullable final Task task = findOneByIndexEntity(userId, index);
        if (task == null) throw new EmptyEntityException();
        entityManager.remove(task);
    }

    @Override
    @SneakyThrows
    public void removeOneByName(
            @NotNull final String userId,
            @Nullable final String name
    ) {
        if (name == null || name.isEmpty()) throw new EmptyIdException();
        @Nullable final Task task = findOneByNameEntity(userId, name);
        if (task == null) throw new EmptyEntityException();
        entityManager.remove(task);
    }

    @Override
    @SneakyThrows
    public void removeOneById(
            @NotNull final String userId,
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Task task = findOneByIdEntity(userId, id);
        if (task == null) throw new EmptyEntityException();
        entityManager.remove(task);
    }

    @Override
    @SneakyThrows
    public Task updateOneById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String newName,
            @NotNull final String newDescription
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Task task = findOneByIdEntity(userId, id);
        task.setName(newName);
        task.setDescription(newDescription);
        entityManager.merge(task);
        return task;
    }

    @Override
    @SneakyThrows
    public Task updateOneByName(
            @NotNull final String userId,
            @NotNull final String oldName,
            @NotNull final String newName,
            @NotNull final String newDescription
    ) {
        @Nullable Task task = findOneByNameEntity(userId, oldName);
        if (task == null) throw new EmptyEntityException();
        task.setName(newName);
        task.setDescription(newDescription);
        entityManager.merge(task);
        return task;
    }

    @Override
    @SneakyThrows
    public Task updateOneByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final String newName,
            @NotNull final String newDescription
    ) {
        @Nullable Task task = findOneByIndexEntity(userId, index);
        if (task == null) throw new EmptyEntityException();
        task.setName(newName);
        task.setDescription(newDescription);
        entityManager.merge(task);
        return task;
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final List<Task> listTasks = getListEntities();
        for (@NotNull final Task task : listTasks) {
            entityManager.remove(task);
        }
    }

    @Override
    @SneakyThrows
    @Nullable
    public List<Task> getList() {
        return getListEntities();
    }

}
