
package ru.anenkov.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the ru.anenkov.tm.endpoint package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CleanDataJson_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "cleanDataJson");
    private final static QName _CleanDataJsonResponse_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "cleanDataJsonResponse");
    private final static QName _ClearDataBase64_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "clearDataBase64");
    private final static QName _ClearDataBase64Response_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "clearDataBase64Response");
    private final static QName _ClearDataBinary_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "clearDataBinary");
    private final static QName _ClearDataBinaryResponse_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "clearDataBinaryResponse");
    private final static QName _ClearDataXml_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "clearDataXml");
    private final static QName _ClearDataXmlResponse_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "clearDataXmlResponse");
    private final static QName _DeleteUserByLogin_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "deleteUserByLogin");
    private final static QName _DeleteUserByLoginResponse_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "deleteUserByLoginResponse");
    private final static QName _LoadDataBase64_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "loadDataBase64");
    private final static QName _LoadDataBase64Response_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "loadDataBase64Response");
    private final static QName _LoadDataBinary_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "loadDataBinary");
    private final static QName _LoadDataBinaryResponse_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "loadDataBinaryResponse");
    private final static QName _LoadDataJson_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "loadDataJson");
    private final static QName _LoadDataJsonResponse_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "loadDataJsonResponse");
    private final static QName _LoadDataXml_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "loadDataXml");
    private final static QName _LoadDataXmlResponse_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "loadDataXmlResponse");
    private final static QName _LockUserByLogin_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "lockUserByLogin");
    private final static QName _LockUserByLoginResponse_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "lockUserByLoginResponse");
    private final static QName _RemoveByEmailUser_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "removeByEmailUser");
    private final static QName _RemoveByEmailUserResponse_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "removeByEmailUserResponse");
    private final static QName _RemoveByIdUser_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "removeByIdUser");
    private final static QName _RemoveByIdUserResponse_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "removeByIdUserResponse");
    private final static QName _RemoveByLoginUser_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "removeByLoginUser");
    private final static QName _RemoveByLoginUserResponse_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "removeByLoginUserResponse");
    private final static QName _RemoveUser_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "removeUser");
    private final static QName _RemoveUserResponse_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "removeUserResponse");
    private final static QName _SaveDataBase64_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "saveDataBase64");
    private final static QName _SaveDataBase64Response_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "saveDataBase64Response");
    private final static QName _SaveDataBinary_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "saveDataBinary");
    private final static QName _SaveDataBinaryResponse_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "saveDataBinaryResponse");
    private final static QName _SaveDataJson_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "saveDataJson");
    private final static QName _SaveDataJsonResponse_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "saveDataJsonResponse");
    private final static QName _SaveDataXml_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "saveDataXml");
    private final static QName _SaveDataXmlResponse_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "saveDataXmlResponse");
    private final static QName _UnlockUserByLogin_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "unlockUserByLogin");
    private final static QName _UnlockUserByLoginResponse_QNAME = new QName("http://endpoint.tm.anenkov.ru/", "unlockUserByLoginResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.anenkov.tm.endpoint
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CleanDataJson }
     */
    public CleanDataJson createCleanDataJson() {
        return new CleanDataJson();
    }

    /**
     * Create an instance of {@link CleanDataJsonResponse }
     */
    public CleanDataJsonResponse createCleanDataJsonResponse() {
        return new CleanDataJsonResponse();
    }

    /**
     * Create an instance of {@link ClearDataBase64 }
     */
    public ClearDataBase64 createClearDataBase64() {
        return new ClearDataBase64();
    }

    /**
     * Create an instance of {@link ClearDataBase64Response }
     */
    public ClearDataBase64Response createClearDataBase64Response() {
        return new ClearDataBase64Response();
    }

    /**
     * Create an instance of {@link ClearDataBinary }
     */
    public ClearDataBinary createClearDataBinary() {
        return new ClearDataBinary();
    }

    /**
     * Create an instance of {@link ClearDataBinaryResponse }
     */
    public ClearDataBinaryResponse createClearDataBinaryResponse() {
        return new ClearDataBinaryResponse();
    }

    /**
     * Create an instance of {@link ClearDataXml }
     */
    public ClearDataXml createClearDataXml() {
        return new ClearDataXml();
    }

    /**
     * Create an instance of {@link ClearDataXmlResponse }
     */
    public ClearDataXmlResponse createClearDataXmlResponse() {
        return new ClearDataXmlResponse();
    }

    /**
     * Create an instance of {@link DeleteUserByLogin }
     */
    public DeleteUserByLogin createDeleteUserByLogin() {
        return new DeleteUserByLogin();
    }

    /**
     * Create an instance of {@link DeleteUserByLoginResponse }
     */
    public DeleteUserByLoginResponse createDeleteUserByLoginResponse() {
        return new DeleteUserByLoginResponse();
    }

    /**
     * Create an instance of {@link LoadDataBase64 }
     */
    public LoadDataBase64 createLoadDataBase64() {
        return new LoadDataBase64();
    }

    /**
     * Create an instance of {@link LoadDataBase64Response }
     */
    public LoadDataBase64Response createLoadDataBase64Response() {
        return new LoadDataBase64Response();
    }

    /**
     * Create an instance of {@link LoadDataBinary }
     */
    public LoadDataBinary createLoadDataBinary() {
        return new LoadDataBinary();
    }

    /**
     * Create an instance of {@link LoadDataBinaryResponse }
     */
    public LoadDataBinaryResponse createLoadDataBinaryResponse() {
        return new LoadDataBinaryResponse();
    }

    /**
     * Create an instance of {@link LoadDataJson }
     */
    public LoadDataJson createLoadDataJson() {
        return new LoadDataJson();
    }

    /**
     * Create an instance of {@link LoadDataJsonResponse }
     */
    public LoadDataJsonResponse createLoadDataJsonResponse() {
        return new LoadDataJsonResponse();
    }

    /**
     * Create an instance of {@link LoadDataXml }
     */
    public LoadDataXml createLoadDataXml() {
        return new LoadDataXml();
    }

    /**
     * Create an instance of {@link LoadDataXmlResponse }
     */
    public LoadDataXmlResponse createLoadDataXmlResponse() {
        return new LoadDataXmlResponse();
    }

    /**
     * Create an instance of {@link LockUserByLogin }
     */
    public LockUserByLogin createLockUserByLogin() {
        return new LockUserByLogin();
    }

    /**
     * Create an instance of {@link LockUserByLoginResponse }
     */
    public LockUserByLoginResponse createLockUserByLoginResponse() {
        return new LockUserByLoginResponse();
    }

    /**
     * Create an instance of {@link RemoveByEmailUser }
     */
    public RemoveByEmailUser createRemoveByEmailUser() {
        return new RemoveByEmailUser();
    }

    /**
     * Create an instance of {@link RemoveByEmailUserResponse }
     */
    public RemoveByEmailUserResponse createRemoveByEmailUserResponse() {
        return new RemoveByEmailUserResponse();
    }

    /**
     * Create an instance of {@link RemoveByIdUser }
     */
    public RemoveByIdUser createRemoveByIdUser() {
        return new RemoveByIdUser();
    }

    /**
     * Create an instance of {@link RemoveByIdUserResponse }
     */
    public RemoveByIdUserResponse createRemoveByIdUserResponse() {
        return new RemoveByIdUserResponse();
    }

    /**
     * Create an instance of {@link RemoveByLoginUser }
     */
    public RemoveByLoginUser createRemoveByLoginUser() {
        return new RemoveByLoginUser();
    }

    /**
     * Create an instance of {@link RemoveByLoginUserResponse }
     */
    public RemoveByLoginUserResponse createRemoveByLoginUserResponse() {
        return new RemoveByLoginUserResponse();
    }

    /**
     * Create an instance of {@link RemoveUser }
     */
    public RemoveUser createRemoveUser() {
        return new RemoveUser();
    }

    /**
     * Create an instance of {@link RemoveUserResponse }
     */
    public RemoveUserResponse createRemoveUserResponse() {
        return new RemoveUserResponse();
    }

    /**
     * Create an instance of {@link SaveDataBase64 }
     */
    public SaveDataBase64 createSaveDataBase64() {
        return new SaveDataBase64();
    }

    /**
     * Create an instance of {@link SaveDataBase64Response }
     */
    public SaveDataBase64Response createSaveDataBase64Response() {
        return new SaveDataBase64Response();
    }

    /**
     * Create an instance of {@link SaveDataBinary }
     */
    public SaveDataBinary createSaveDataBinary() {
        return new SaveDataBinary();
    }

    /**
     * Create an instance of {@link SaveDataBinaryResponse }
     */
    public SaveDataBinaryResponse createSaveDataBinaryResponse() {
        return new SaveDataBinaryResponse();
    }

    /**
     * Create an instance of {@link SaveDataJson }
     */
    public SaveDataJson createSaveDataJson() {
        return new SaveDataJson();
    }

    /**
     * Create an instance of {@link SaveDataJsonResponse }
     */
    public SaveDataJsonResponse createSaveDataJsonResponse() {
        return new SaveDataJsonResponse();
    }

    /**
     * Create an instance of {@link SaveDataXml }
     */
    public SaveDataXml createSaveDataXml() {
        return new SaveDataXml();
    }

    /**
     * Create an instance of {@link SaveDataXmlResponse }
     */
    public SaveDataXmlResponse createSaveDataXmlResponse() {
        return new SaveDataXmlResponse();
    }

    /**
     * Create an instance of {@link UnlockUserByLogin }
     */
    public UnlockUserByLogin createUnlockUserByLogin() {
        return new UnlockUserByLogin();
    }

    /**
     * Create an instance of {@link UnlockUserByLoginResponse }
     */
    public UnlockUserByLoginResponse createUnlockUserByLoginResponse() {
        return new UnlockUserByLoginResponse();
    }

    /**
     * Create an instance of {@link SessionDTO }
     */
    public SessionDTO createSessionDTO() {
        return new SessionDTO();
    }

    /**
     * Create an instance of {@link UserDTO }
     */
    public UserDTO createUserDTO() {
        return new UserDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CleanDataJson }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "cleanDataJson")
    public JAXBElement<CleanDataJson> createCleanDataJson(CleanDataJson value) {
        return new JAXBElement<CleanDataJson>(_CleanDataJson_QNAME, CleanDataJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CleanDataJsonResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "cleanDataJsonResponse")
    public JAXBElement<CleanDataJsonResponse> createCleanDataJsonResponse(CleanDataJsonResponse value) {
        return new JAXBElement<CleanDataJsonResponse>(_CleanDataJsonResponse_QNAME, CleanDataJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearDataBase64 }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "clearDataBase64")
    public JAXBElement<ClearDataBase64> createClearDataBase64(ClearDataBase64 value) {
        return new JAXBElement<ClearDataBase64>(_ClearDataBase64_QNAME, ClearDataBase64.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearDataBase64Response }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "clearDataBase64Response")
    public JAXBElement<ClearDataBase64Response> createClearDataBase64Response(ClearDataBase64Response value) {
        return new JAXBElement<ClearDataBase64Response>(_ClearDataBase64Response_QNAME, ClearDataBase64Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearDataBinary }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "clearDataBinary")
    public JAXBElement<ClearDataBinary> createClearDataBinary(ClearDataBinary value) {
        return new JAXBElement<ClearDataBinary>(_ClearDataBinary_QNAME, ClearDataBinary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearDataBinaryResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "clearDataBinaryResponse")
    public JAXBElement<ClearDataBinaryResponse> createClearDataBinaryResponse(ClearDataBinaryResponse value) {
        return new JAXBElement<ClearDataBinaryResponse>(_ClearDataBinaryResponse_QNAME, ClearDataBinaryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearDataXml }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "clearDataXml")
    public JAXBElement<ClearDataXml> createClearDataXml(ClearDataXml value) {
        return new JAXBElement<ClearDataXml>(_ClearDataXml_QNAME, ClearDataXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearDataXmlResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "clearDataXmlResponse")
    public JAXBElement<ClearDataXmlResponse> createClearDataXmlResponse(ClearDataXmlResponse value) {
        return new JAXBElement<ClearDataXmlResponse>(_ClearDataXmlResponse_QNAME, ClearDataXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteUserByLogin }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "deleteUserByLogin")
    public JAXBElement<DeleteUserByLogin> createDeleteUserByLogin(DeleteUserByLogin value) {
        return new JAXBElement<DeleteUserByLogin>(_DeleteUserByLogin_QNAME, DeleteUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteUserByLoginResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "deleteUserByLoginResponse")
    public JAXBElement<DeleteUserByLoginResponse> createDeleteUserByLoginResponse(DeleteUserByLoginResponse value) {
        return new JAXBElement<DeleteUserByLoginResponse>(_DeleteUserByLoginResponse_QNAME, DeleteUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataBase64 }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "loadDataBase64")
    public JAXBElement<LoadDataBase64> createLoadDataBase64(LoadDataBase64 value) {
        return new JAXBElement<LoadDataBase64>(_LoadDataBase64_QNAME, LoadDataBase64.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataBase64Response }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "loadDataBase64Response")
    public JAXBElement<LoadDataBase64Response> createLoadDataBase64Response(LoadDataBase64Response value) {
        return new JAXBElement<LoadDataBase64Response>(_LoadDataBase64Response_QNAME, LoadDataBase64Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataBinary }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "loadDataBinary")
    public JAXBElement<LoadDataBinary> createLoadDataBinary(LoadDataBinary value) {
        return new JAXBElement<LoadDataBinary>(_LoadDataBinary_QNAME, LoadDataBinary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataBinaryResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "loadDataBinaryResponse")
    public JAXBElement<LoadDataBinaryResponse> createLoadDataBinaryResponse(LoadDataBinaryResponse value) {
        return new JAXBElement<LoadDataBinaryResponse>(_LoadDataBinaryResponse_QNAME, LoadDataBinaryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataJson }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "loadDataJson")
    public JAXBElement<LoadDataJson> createLoadDataJson(LoadDataJson value) {
        return new JAXBElement<LoadDataJson>(_LoadDataJson_QNAME, LoadDataJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataJsonResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "loadDataJsonResponse")
    public JAXBElement<LoadDataJsonResponse> createLoadDataJsonResponse(LoadDataJsonResponse value) {
        return new JAXBElement<LoadDataJsonResponse>(_LoadDataJsonResponse_QNAME, LoadDataJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataXml }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "loadDataXml")
    public JAXBElement<LoadDataXml> createLoadDataXml(LoadDataXml value) {
        return new JAXBElement<LoadDataXml>(_LoadDataXml_QNAME, LoadDataXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataXmlResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "loadDataXmlResponse")
    public JAXBElement<LoadDataXmlResponse> createLoadDataXmlResponse(LoadDataXmlResponse value) {
        return new JAXBElement<LoadDataXmlResponse>(_LoadDataXmlResponse_QNAME, LoadDataXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LockUserByLogin }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "lockUserByLogin")
    public JAXBElement<LockUserByLogin> createLockUserByLogin(LockUserByLogin value) {
        return new JAXBElement<LockUserByLogin>(_LockUserByLogin_QNAME, LockUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LockUserByLoginResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "lockUserByLoginResponse")
    public JAXBElement<LockUserByLoginResponse> createLockUserByLoginResponse(LockUserByLoginResponse value) {
        return new JAXBElement<LockUserByLoginResponse>(_LockUserByLoginResponse_QNAME, LockUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveByEmailUser }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "removeByEmailUser")
    public JAXBElement<RemoveByEmailUser> createRemoveByEmailUser(RemoveByEmailUser value) {
        return new JAXBElement<RemoveByEmailUser>(_RemoveByEmailUser_QNAME, RemoveByEmailUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveByEmailUserResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "removeByEmailUserResponse")
    public JAXBElement<RemoveByEmailUserResponse> createRemoveByEmailUserResponse(RemoveByEmailUserResponse value) {
        return new JAXBElement<RemoveByEmailUserResponse>(_RemoveByEmailUserResponse_QNAME, RemoveByEmailUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveByIdUser }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "removeByIdUser")
    public JAXBElement<RemoveByIdUser> createRemoveByIdUser(RemoveByIdUser value) {
        return new JAXBElement<RemoveByIdUser>(_RemoveByIdUser_QNAME, RemoveByIdUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveByIdUserResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "removeByIdUserResponse")
    public JAXBElement<RemoveByIdUserResponse> createRemoveByIdUserResponse(RemoveByIdUserResponse value) {
        return new JAXBElement<RemoveByIdUserResponse>(_RemoveByIdUserResponse_QNAME, RemoveByIdUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveByLoginUser }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "removeByLoginUser")
    public JAXBElement<RemoveByLoginUser> createRemoveByLoginUser(RemoveByLoginUser value) {
        return new JAXBElement<RemoveByLoginUser>(_RemoveByLoginUser_QNAME, RemoveByLoginUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveByLoginUserResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "removeByLoginUserResponse")
    public JAXBElement<RemoveByLoginUserResponse> createRemoveByLoginUserResponse(RemoveByLoginUserResponse value) {
        return new JAXBElement<RemoveByLoginUserResponse>(_RemoveByLoginUserResponse_QNAME, RemoveByLoginUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUser }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "removeUser")
    public JAXBElement<RemoveUser> createRemoveUser(RemoveUser value) {
        return new JAXBElement<RemoveUser>(_RemoveUser_QNAME, RemoveUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "removeUserResponse")
    public JAXBElement<RemoveUserResponse> createRemoveUserResponse(RemoveUserResponse value) {
        return new JAXBElement<RemoveUserResponse>(_RemoveUserResponse_QNAME, RemoveUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataBase64 }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "saveDataBase64")
    public JAXBElement<SaveDataBase64> createSaveDataBase64(SaveDataBase64 value) {
        return new JAXBElement<SaveDataBase64>(_SaveDataBase64_QNAME, SaveDataBase64.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataBase64Response }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "saveDataBase64Response")
    public JAXBElement<SaveDataBase64Response> createSaveDataBase64Response(SaveDataBase64Response value) {
        return new JAXBElement<SaveDataBase64Response>(_SaveDataBase64Response_QNAME, SaveDataBase64Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataBinary }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "saveDataBinary")
    public JAXBElement<SaveDataBinary> createSaveDataBinary(SaveDataBinary value) {
        return new JAXBElement<SaveDataBinary>(_SaveDataBinary_QNAME, SaveDataBinary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataBinaryResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "saveDataBinaryResponse")
    public JAXBElement<SaveDataBinaryResponse> createSaveDataBinaryResponse(SaveDataBinaryResponse value) {
        return new JAXBElement<SaveDataBinaryResponse>(_SaveDataBinaryResponse_QNAME, SaveDataBinaryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataJson }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "saveDataJson")
    public JAXBElement<SaveDataJson> createSaveDataJson(SaveDataJson value) {
        return new JAXBElement<SaveDataJson>(_SaveDataJson_QNAME, SaveDataJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataJsonResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "saveDataJsonResponse")
    public JAXBElement<SaveDataJsonResponse> createSaveDataJsonResponse(SaveDataJsonResponse value) {
        return new JAXBElement<SaveDataJsonResponse>(_SaveDataJsonResponse_QNAME, SaveDataJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataXml }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "saveDataXml")
    public JAXBElement<SaveDataXml> createSaveDataXml(SaveDataXml value) {
        return new JAXBElement<SaveDataXml>(_SaveDataXml_QNAME, SaveDataXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataXmlResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "saveDataXmlResponse")
    public JAXBElement<SaveDataXmlResponse> createSaveDataXmlResponse(SaveDataXmlResponse value) {
        return new JAXBElement<SaveDataXmlResponse>(_SaveDataXmlResponse_QNAME, SaveDataXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnlockUserByLogin }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "unlockUserByLogin")
    public JAXBElement<UnlockUserByLogin> createUnlockUserByLogin(UnlockUserByLogin value) {
        return new JAXBElement<UnlockUserByLogin>(_UnlockUserByLogin_QNAME, UnlockUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnlockUserByLoginResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.anenkov.ru/", name = "unlockUserByLoginResponse")
    public JAXBElement<UnlockUserByLoginResponse> createUnlockUserByLoginResponse(UnlockUserByLoginResponse value) {
        return new JAXBElement<UnlockUserByLoginResponse>(_UnlockUserByLoginResponse_QNAME, UnlockUserByLoginResponse.class, null, value);
    }

}
