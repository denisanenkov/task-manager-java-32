package ru.anenkov.tm.listener.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.endpoint.AdminEndpoint;
import ru.anenkov.tm.enumeration.Role;

@Component
public class DataBase64LoadListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String command() {
        return "Data-base64-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load base64 date.";
    }

    @Override
    @EventListener(condition = "@dataBase64LoadListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[DATA BASE64 LOAD]");
        adminEndpoint.loadDataBase64(bootstrap.getSession());
        System.out.println("[SUCCESS]");
    }

}
