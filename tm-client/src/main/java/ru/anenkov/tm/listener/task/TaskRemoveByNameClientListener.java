package ru.anenkov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.endpoint.TaskEndpoint;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

@Component
public class TaskRemoveByNameClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Task-remove-by-name";
    }

    @Override
    public @Nullable String description() {
        return "Task remove by name";
    }

    @Override
    @EventListener(condition = "@taskRemoveByNameClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[DELETE TASK]");
        System.out.print("ENTER NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        taskEndpoint.removeOneByName(bootstrap.getSession(), name);
        System.out.println("[DELETE SUCCESS]");
    }

}
