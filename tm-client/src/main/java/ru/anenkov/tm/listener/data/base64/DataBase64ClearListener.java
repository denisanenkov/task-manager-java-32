package ru.anenkov.tm.listener.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.endpoint.AdminEndpoint;
import ru.anenkov.tm.enumeration.Role;

@Component
public class DataBase64ClearListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String command() {
        return "Data-base64-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Clear base64 data.";
    }

    @Override
    @EventListener(condition = "@dataBase64ClearListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[DATA BASE64 CLEAR]");
        adminEndpoint.clearDataBase64(bootstrap.getSession());
        System.out.println("[SUCCESS]");
    }

}

