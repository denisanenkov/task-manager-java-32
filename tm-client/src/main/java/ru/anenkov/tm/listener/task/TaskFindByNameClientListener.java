package ru.anenkov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.endpoint.TaskDTO;
import ru.anenkov.tm.endpoint.TaskEndpoint;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.exception.system.IncorrectDataException;
import ru.anenkov.tm.util.TerminalUtil;

@Component
public class TaskFindByNameClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Show-task-by-name";
    }

    @Override
    public @Nullable String description() {
        return "Show Task By Name";
    }

    @Override
    @EventListener(condition = "@taskFindByNameClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[SHOW TASK]");
        System.out.print("ENTER NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        @Nullable final TaskDTO task = taskEndpoint.findOneByName(bootstrap.getSession(), name);
        if (task == null) throw new IncorrectDataException();
        System.out.println("" +
                "NAME: " + task.getName() +
                ", \nDESCRIPTION: " + task.getDescription() +
                ", \nUSER ID: " + task.getUserId()
        );
        System.out.println("[SUCCESS]");
    }

}
