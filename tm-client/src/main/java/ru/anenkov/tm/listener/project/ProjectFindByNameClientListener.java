package ru.anenkov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.endpoint.ProjectDTO;
import ru.anenkov.tm.endpoint.ProjectEndpoint;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.exception.system.IncorrectDataException;
import ru.anenkov.tm.util.TerminalUtil;

@Component
public class ProjectFindByNameClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Show-project-by-name";
    }

    @Override
    public @Nullable String description() {
        return "Show project By Name";
    }

    @Override
    @EventListener(condition = "@projectFindByNameClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[SHOW PROJECT]");
        System.out.print("ENTER NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        @Nullable final ProjectDTO project = projectEndpoint.findOneByNameProject(bootstrap.getSession(), name);
        if (project == null) throw new IncorrectDataException();
        System.out.println("" +
                "NAME: " + project.getName() +
                ", \nDESCRIPTION: " + project.getDescription() +
                ", \nUSER ID: " + project.getUserId()
        );
        System.out.println("[SUCCESS]");
    }

}

